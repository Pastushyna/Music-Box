package com._3rdStepProject._8thGroup.controller;

import com._3rdStepProject._8thGroup.model.Album;
import com._3rdStepProject._8thGroup.service.AlbumsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;
import java.util.List;

@RestController
@RequestMapping(path = "/api/albums")
public class AlbumsController {

  @Autowired
  private AlbumsService albumsService;

  @GetMapping("/")
  @CrossOrigin("http://localhost:3000")
  public ResponseEntity<List<Album>> getAllAlbums() {
    List<Album> albums = albumsService.getAllAlbum();
    return ResponseEntity.ok().body(albums);
  }

  @GetMapping("/{id}")
  @CrossOrigin("http://localhost:3000")
  public ResponseEntity<Album> getAlbumById(@PathVariable("id") Long id) {
    Album album = albumsService.getAlbumById(id);
    return ResponseEntity.ok().body(album);
  }

  @PostMapping("/secret/map")
  @Transactional
  @CrossOrigin("http://localhost:3000")
  public ResponseEntity<?> createAlbum(@RequestBody Album album) {
    System.out.println(album.getAlbumName());
    albumsService.addAlbum(album);
    return ResponseEntity.ok().body("New album added with ID: " + album.getAlbumId());
  }

  @PutMapping("/{id}")
  @CrossOrigin("http://localhost:3000")
  public ResponseEntity<?> updateAlbumById(@PathVariable("id") Long id, @RequestBody Album album){
    albumsService.updateAlbumById(id, album);
    return ResponseEntity.ok().body("Album updated");
  }

  @DeleteMapping("/{id}")
  @CrossOrigin("http://localhost:3000")
  public ResponseEntity<?> deleteAlbumById(@PathVariable("id") Long id){
    albumsService.deleteAlbumById(id);
    return ResponseEntity.ok().body("Album deleted");
  }

  @DeleteMapping("/{id}/cover")
  @CrossOrigin("http://localhost:3000")
  public ResponseEntity<?> deleteCoverOfAlbumById(@PathVariable("id") Long id){
    albumsService.deleteCoverOfAlbum(id);
    return ResponseEntity.ok().body("Cover deleted");
  }
/*
  @GetMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
  @CrossOrigin(value = "http://localhost:3000")
  public ResponseEntity<List<Album>> getAllAlbums (){

    return new ResponseEntity<>(albumsService.getAllAlbums(), HttpStatus.OK);

  }*/

  @GetMapping(path = "/error")
  public ResponseEntity<String> testError()  {

    return new ResponseEntity<>(albumsService.testError(), HttpStatus.OK);
  }

}
