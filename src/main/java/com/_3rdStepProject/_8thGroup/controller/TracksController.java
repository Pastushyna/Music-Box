package com._3rdStepProject._8thGroup.controller;

import com._3rdStepProject._8thGroup.model.Track;
import com._3rdStepProject._8thGroup.service.TracksService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/tracks")
public class TracksController {

  @Autowired
  private TracksService tracksService;

  @GetMapping("/")
  @CrossOrigin("http://localhost:3000")
  public ResponseEntity<List<Track>> getAllTracks() {
    List<Track> tracks = tracksService.getAllTrack();
    return ResponseEntity.ok().body(tracks);
  }

  @GetMapping("/{id}")
  @CrossOrigin("http://localhost:3000")
  public ResponseEntity<Track> getTrackById(@PathVariable("id") Long id) {
    Track track = tracksService.getTrackById(id);
    return ResponseEntity.ok().body(track);
  }

  @PostMapping("/secret/map")
  @CrossOrigin("http://localhost:3000")
  public ResponseEntity<?> createTrack(@RequestBody Track track) {
    Long trackId = tracksService.addTrack(track);
    return ResponseEntity.ok().body("New track added with ID: " + trackId);
  }

  @PutMapping("/{id}")
  @CrossOrigin("http://localhost:3000")
  public ResponseEntity<?> updateTrackById(@PathVariable("id") Long id, @RequestBody Track track){
    tracksService.updateTrack(id, track);
    return ResponseEntity.ok().body("Track updated");
  }

  @DeleteMapping("/{id}")
  @CrossOrigin("http://localhost:3000")
  public ResponseEntity<?> deleteTrackById(@PathVariable("id") Long id){
    tracksService.deleteTrackById(id);
    return ResponseEntity.ok().body("Track deleted");
  }
}
