package com._3rdStepProject._8thGroup.dao;

import com._3rdStepProject._8thGroup.model.Album;

import java.util.List;

public interface AlbumsDao {

  List<Album> getAllAlbums();

  void addAlbum(Album album);

  Album getAlbumById(Long albumId);

  Album getAlbumByName(String albumName);

  Album getAlbumByIdAndName(Long albumId, String albumName);

  List<Album> getAllAlbumsNew();

}
