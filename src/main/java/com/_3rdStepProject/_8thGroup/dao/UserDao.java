package com._3rdStepProject._8thGroup.dao;

import com._3rdStepProject._8thGroup.model.User;

import java.util.List;

public interface UserDao {

  List<User> getAllTracks();

  void addTrack(User user);

  User getTrackById(Long trackId);

  User getTrackByName(String trackName);

  User getTrackByIdAndName(Long trackId, String trackName);

}
