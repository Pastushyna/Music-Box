package com._3rdStepProject._8thGroup.dao;

import com._3rdStepProject._8thGroup.model.Album;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import java.util.List;

@Repository
@Transactional
public class AlbumsDaoMySqlImpl implements AlbumsDao {

  @Autowired
  private EntityManager entityManager;

  @Override
  public List<Album> getAllAlbums() {
    return entityManager
        .createNativeQuery("SELECT * FROM albums")
        .getResultList();
  }

  @Override
  public void addAlbum(Album album) {
    entityManager.persist(album);
  }

  @Override
  public Album getAlbumById(Long albumId) {

    String sql = "SELECT albums.* FROM albums WHERE album_id IN (" + albumId + ")" ;
    Query query = entityManager.createNativeQuery(sql, Album.class);
    Album album = (Album) query.getSingleResult();
    return album;
  }

  @Override
  public Album getAlbumByName(String albumName) {
    return null;
  }

  @Override
  public Album getAlbumByIdAndName(Long albumId, String albumName) {
    return null;
  }

  @Override
  public List<Album> getAllAlbumsNew() {
    Query query = entityManager.createNativeQuery("SELECT * FROM albums", Album.class);
    List<Album> list = (List<Album>) query.getResultList();
    return list;
  }
}
