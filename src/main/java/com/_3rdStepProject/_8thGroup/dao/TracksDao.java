package com._3rdStepProject._8thGroup.dao;

import com._3rdStepProject._8thGroup.model.Track;

import java.util.List;

public interface TracksDao {

  List<Track> getAllTracks();

  void addTrack(Track track);

  Track getTrackById(Long trackId);

  Track getTrackByName(String trackName);

  Track getTrackByIdAndName(Long trackId, String trackName);

}
