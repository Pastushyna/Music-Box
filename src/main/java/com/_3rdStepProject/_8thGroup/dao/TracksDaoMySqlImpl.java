package com._3rdStepProject._8thGroup.dao;

import com._3rdStepProject._8thGroup.model.Track;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

import java.util.List;

@Repository
@Transactional
public class TracksDaoMySqlImpl implements TracksDao {

  @Autowired
  private EntityManager entityManager;

  @Override
  public List<Track> getAllTracks() {
    return entityManager
        .createNativeQuery("SELECT * FROM tracks")
        .getResultList();
  }

  @Override
  public void addTrack(Track track) {
  }

  @Override
  public Track getTrackById(Long trackId) {
    return null;
  }

  @Override
  public Track getTrackByName(String trackName) {
    return null;
  }

  @Override
  public Track getTrackByIdAndName(Long trackId, String trackName) {
    return null;
  }
}
