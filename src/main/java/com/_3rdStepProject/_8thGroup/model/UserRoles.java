package com._3rdStepProject._8thGroup.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "roles")
public class UserRoles {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "roles_id")
  private Long rolesId;

  @Column(name = "role_name")
  private String roleName;

  @ManyToMany(mappedBy = "roles", cascade = CascadeType.PERSIST)
  private List<User> users = new ArrayList<>();

  @ManyToMany(cascade = CascadeType.PERSIST)
  @JoinTable(name = "role_permission", joinColumns = @JoinColumn(name = "id_role", referencedColumnName = "roles_id"),
      inverseJoinColumns = @JoinColumn(name = "id_permission", referencedColumnName = "privileges_id"))
  private List<UserPrivileges> privileges = new ArrayList<>();

  public Long getRolesId() {
    return rolesId;
  }

  public void setRolesId(Long rolesId) {
    this.rolesId = rolesId;
  }

  public String getRoleName() {
    return roleName;
  }

  public void setRoleName(String roleName) {
    this.roleName = roleName;
  }

  public Collection<User> getUsers() {
    return users;
  }

  public void setUsers(List<User> users) {
    this.users = users;
  }

  public Collection<UserPrivileges> getPrivileges() {
    return privileges;
  }

  public void setPrivileges(List<UserPrivileges> privileges) {
    this.privileges = privileges;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    UserRoles userRoles = (UserRoles) o;
    return Objects.equals(rolesId, userRoles.rolesId) &&
        Objects.equals(roleName, userRoles.roleName) &&
        Objects.equals(users, userRoles.users) &&
        Objects.equals(privileges, userRoles.privileges);
  }

  @Override
  public int hashCode() {

    return Objects.hash(rolesId, roleName, users, privileges);
  }
}
