package com._3rdStepProject._8thGroup.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "albums")
public class Album {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "album_id")
  private Long albumId;

  @Column(name = "artist_name")
  private String artistName;

  @Column(name = "album_name")
  private String albumName;

  @Column(name = "album_path")
  private String coverPath;

  @OneToMany(targetEntity = Track.class, mappedBy = "album", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<Track> tracks = new ArrayList<>();

  public Long getAlbumId() {
    return albumId;
  }

  public void setAlbumId(Long albumId) {
    this.albumId = albumId;
  }

  public String getArtistName() {
    return artistName;
  }

  public void setArtistName(String artistName) {
    this.artistName = artistName;
  }

  public String getAlbumName() {
    return albumName;
  }

  public void setAlbumName(String albumName) {
    this.albumName = albumName;
  }

  public String getCoverPath() {
    return coverPath;
  }

  public void setCoverPath(String coverPath) {
    this.coverPath = coverPath;
  }

  public List<Track> getTracks() {
    return tracks;
  }

  public void setTracks(List<Track> tracks) {
    this.tracks = tracks;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Album album = (Album) o;
    return Objects.equals(albumId, album.albumId) &&
        Objects.equals(artistName, album.artistName) &&
        Objects.equals(albumName, album.albumName) &&
        Objects.equals(coverPath, album.coverPath) &&
        Objects.equals(tracks, album.tracks);
  }

  @Override
  public int hashCode() {

    return Objects.hash(albumId, artistName, albumName, coverPath, tracks);
  }


}
