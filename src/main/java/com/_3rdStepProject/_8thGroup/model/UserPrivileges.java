package com._3rdStepProject._8thGroup.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "permissions")
public class UserPrivileges {

  @Id
  @GeneratedValue(strategy =  GenerationType.AUTO)
  @Column(name = "privileges_id")
  private Long privilegesId;

  @Column(name = "privileges_name")
  private String privilegesName;

  @ManyToMany(mappedBy = "privileges", cascade = CascadeType.PERSIST)
  private List<UserRoles> roles = new ArrayList<>();

  public Long getPrivilegesId() {
    return privilegesId;
  }

  public void setPrivilegesId(Long privilegesId) {
    this.privilegesId = privilegesId;
  }

  public String getPrivilegesName() {
    return privilegesName;
  }

  public void setPrivilegesName(String privilegesName) {
    this.privilegesName = privilegesName;
  }

  public List<UserRoles> getRoles() {
    return roles;
  }

  public void setRoles(List<UserRoles> roles) {
    this.roles = roles;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    UserPrivileges that = (UserPrivileges) o;
    return Objects.equals(privilegesId, that.privilegesId) &&
        Objects.equals(privilegesName, that.privilegesName) &&
        Objects.equals(roles, that.roles);
  }

  @Override
  public int hashCode() {

    return Objects.hash(privilegesId, privilegesName, roles);
  }
}
