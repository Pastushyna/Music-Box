package com._3rdStepProject._8thGroup.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "users")
public class User {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "user_id")
  private Long userId;

  @Column(name = "user_login")
  private String userLogin;

  @Column(name = "user_email")
  private String userEmail;

  @Column(name = "user_name")
  private String userName;

  @Column(name = "user_lastname")
  private String userLastName;

  @Column(name = "userPassword")
  @JsonIgnore
  private String userPassword;

  @Column(name = "user_enabled")
  private Boolean userEnabled;

  @ManyToMany(cascade = CascadeType.PERSIST)
  @JoinTable(name = "tracks_favorite", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "user_id"),
      inverseJoinColumns = @JoinColumn(name = "track_id", referencedColumnName = "track_id"))
  private List<Track> tracksUser = new ArrayList<>();

  @ManyToMany(cascade = CascadeType.PERSIST)
  @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "id_user", referencedColumnName = "user_id"),
      inverseJoinColumns = @JoinColumn(name = "id_role", referencedColumnName = "roles_id"))
  private List<UserRoles> roles = new ArrayList<>();

  public Boolean getUserEnabled() {
    return userEnabled;
  }

  public void setUserEnabled(Boolean userEnabled) {
    this.userEnabled = userEnabled;
  }

  public List<Track> getTracksUser() {
    return tracksUser;
  }

  public void setTracksUser(List<Track> tracksUser) {
    this.tracksUser = tracksUser;
  }

  public List<UserRoles> getRoles() {
    return roles;
  }

  public void setRoles(List<UserRoles> roles) {
    this.roles = roles;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getUserLogin() {
    return userLogin;
  }

  public void setUserLogin(String userLogin) {
    this.userLogin = userLogin;
  }

  public String getUserEmail() {
    return userEmail;
  }

  public void setUserEmail(String userEmail) {
    this.userEmail = userEmail;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getUserLastName() {
    return userLastName;
  }

  public void setUserLastName(String userLastName) {
    this.userLastName = userLastName;
  }

  public String getUserPassword() {
    return userPassword;
  }

  public void setUserPassword(String userPassword) {
    this.userPassword = userPassword;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    User user = (User) o;
    return Objects.equals(userId, user.userId) &&
        Objects.equals(userLogin, user.userLogin) &&
        Objects.equals(userEmail, user.userEmail) &&
        Objects.equals(userName, user.userName) &&
        Objects.equals(userLastName, user.userLastName) &&
        Objects.equals(userPassword, user.userPassword) &&
        Objects.equals(userEnabled, user.userEnabled) &&
        Objects.equals(tracksUser, user.tracksUser) &&
        Objects.equals(roles, user.roles);
  }

  @Override
  public int hashCode() {

    return Objects.hash(userId, userLogin, userEmail, userName, userLastName, userPassword, userEnabled, tracksUser, roles);
  }
}