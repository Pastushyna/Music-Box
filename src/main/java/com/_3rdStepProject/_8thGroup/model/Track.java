package com._3rdStepProject._8thGroup.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "tracks")
public class Track {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "track_id")
  private Long trackID;

  @Column(name = "track_name")
  private String trackName;

  @Column(name = "track_path")
  private String trackPath;

  @ManyToOne(targetEntity = Album.class, cascade = CascadeType.PERSIST)
  @JoinColumn(name = "id_album", referencedColumnName = "album_id", nullable = false, table = "tracks")
  @JsonIgnoreProperties(value = "tracks", allowSetters = true)
  private Album album;

  @ManyToMany(mappedBy = "tracksUser", cascade = CascadeType.PERSIST)
  private List<User> users = new ArrayList<>();

  public String getTrackPath() {
    return trackPath;
  }

  public void setTrackPath(String trackPath) {
    this.trackPath = trackPath;
  }

  public Long getTrackID() {
    return trackID;
  }

  public void setTrackID(Long trackID) {
    this.trackID = trackID;
  }

  public String getTrackName() {
    return trackName;
  }

  public void setTrackName(String trackName) {
    this.trackName = trackName;
  }

  public Album getAlbum() {
    return album;
  }

  public void setAlbum(Album album) {
    this.album = album;
  }

  public List<User> getUsers() {
    return users;
  }

  public void setUsers(List<User> users) {
    this.users = users;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Track track = (Track) o;
    return Objects.equals(trackID, track.trackID) &&
        Objects.equals(trackName, track.trackName) &&
        Objects.equals(trackPath, track.trackPath) &&
        Objects.equals(album, track.album) &&
        Objects.equals(users, track.users);
  }

  @Override
  public int hashCode() {

    return Objects.hash(trackID, trackName, trackPath, album, users);
  }
}
