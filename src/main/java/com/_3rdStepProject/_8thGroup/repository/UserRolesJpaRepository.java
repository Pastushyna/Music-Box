package com._3rdStepProject._8thGroup.repository;

import com._3rdStepProject._8thGroup.model.UserRoles;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface UserRolesJpaRepository extends JpaRepository<UserRoles, Long>{
}
