package com._3rdStepProject._8thGroup.repository;

import com._3rdStepProject._8thGroup.model.Album;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface AlbumsJpaRepository extends JpaRepository<Album, Long> {

  @Query(value = "SELECT * FROM albums", nativeQuery = true)
  List<Album> getAllAlbums();

  Album getAlbumByAlbumName(String albumName);

  @Query(value = "UPDATE albums SET album_path = null WHERE album_id = :albumId", nativeQuery = true)
  void deleteCoverPathInAlbumById(@Param("albumId") Long albumId);

}
