package com._3rdStepProject._8thGroup.repository;

import com._3rdStepProject._8thGroup.model.Track;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface TracksJpaRepository extends JpaRepository<Track, Long> {

  List<Track> getTracksByAlbum(String albumName);

  Track getTrackByTrackName(String trackName);

  List<Track>findByAlbum_AlbumId(Long albumId);

}



