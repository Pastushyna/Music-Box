package com._3rdStepProject._8thGroup.repository;

import com._3rdStepProject._8thGroup.model.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface UsersJpaRepository extends JpaRepository<User, Long> {

  User getUserByUserLogin(String userLogin);

  User getUserByUserEmail(String userEmail);


}
