package com._3rdStepProject._8thGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Testing {

  public static void main(String[] args) throws InterruptedException {
    String string = "hello";
    StringBuilder builder = new StringBuilder("hello");
    StringBuilder builder2 = new StringBuilder("hello");

    //System.out.println(string == builder);
    System.out.println(string.equals(builder));
    System.out.println(builder.equals(builder2));

    System.out.println("##################################################");

    String s1 = "hello";
    final String s2 = "hel";
    String s3 = "lo";
    String s4 = "hello";

    System.out.println(s1 == s4);
    System.out.println(s1 == s2 + s3);
    System.out.println(s1 == s2 + "lo");

    System.out.println("##################################################");

    Long a = 111L;
    Long b1 = 111L;
    Long c = 222L;
    Long d = 222L;

    System.out.println(a == b1);
    System.out.println(c == d);

    System.out.println("############################################################");

    int[] arr = new int[1];
    int i = 2;
    //arr[--i] = 1/ --i;


    byte b = 1;
    action(b, b);

    Testing testing = new Testing();
    testing.run();
    //testing.work();

    System.out.println("############################################################");

    Thread thread = new Thread(() -> System.out.print("Thread end."));

    synchronized (thread){
      thread.start();
      thread.wait();
    }

    System.out.print("Main end");

    System.out.println("############################################################");

    /*Set<Number> set = new TreeSet<>();
    set.add(1);
    set.add(1L);
    set.add(1.0);

    System.out.println(set.size()); - class cast exception */

    System.out.println("############################################################");

    List longs = new ArrayList<Long>();
    longs.add(1L);
    longs.add(1.0);
    longs.add(new Object());
    longs.add("I am LONG!!");

    System.out.println(longs.size());

    System.out.println("############################################################");

    System.out.print(Values.A);
  }

  enum Values {
    A(1), B(2), C(3);

    Values(int i){
      System.out.print(i);
    }

    static {
      System.out.print("static");
    }
  }

  static void action(byte...args) {}
  static void action(long l1, short s2) {}
  static void action(Byte i1, Byte i2) {}

  private void run() {
    System.out.print("Best conferense:");
    http://www.javapoint.ru
    return;
  }

  private void work(){
    try {
      work();
    } finally {
      work();
    }
  }
}
