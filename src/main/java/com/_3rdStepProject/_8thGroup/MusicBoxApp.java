package com._3rdStepProject._8thGroup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Hello world!
 *
 */

@SpringBootApplication
public class MusicBoxApp
{
    public static void main( String[] args )
    {
        SpringApplication.run(MusicBoxApp.class, args);
    }
}
