package com._3rdStepProject._8thGroup.configuration;

import com._3rdStepProject._8thGroup.model.Track;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;

import java.util.HashSet;
import java.util.Set;

public class CustomSetDeserializer extends StdDeserializer<Set<Track>> {

  public CustomSetDeserializer() {
    this(null);
  }

  public CustomSetDeserializer(Class<?> vc) {
    super(vc);
  }

  @Override
  public Set<Track> deserialize(
      JsonParser jsonparser,
      DeserializationContext context)
      throws IOException, JsonProcessingException {

    return new HashSet<>();
  }
}
