package com._3rdStepProject._8thGroup.configuration;

import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.stereotype.Component;

/*
**  use this class if only 8080 port already under tomcat
**
 */

@Component
public class CustomizationBean implements EmbeddedServletContainerCustomizer {
  @Override
  public void customize(ConfigurableEmbeddedServletContainer container) {
    container.setPort(8080);
  }
}