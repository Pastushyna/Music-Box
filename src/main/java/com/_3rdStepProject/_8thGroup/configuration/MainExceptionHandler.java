package com._3rdStepProject._8thGroup.configuration;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

public class MainExceptionHandler {

  @ExceptionHandler(Exception.class)
  public ResponseEntity<String> handle() {
    return new ResponseEntity<String>
        ("Service temporary unavaible", HttpStatus.SERVICE_UNAVAILABLE);
  }
}
