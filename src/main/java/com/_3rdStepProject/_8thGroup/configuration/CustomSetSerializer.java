package com._3rdStepProject._8thGroup.configuration;

import com._3rdStepProject._8thGroup.model.Track;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class CustomSetSerializer extends StdSerializer<Set<Track>> {

  public CustomSetSerializer() {
    this(null);
  }

  public CustomSetSerializer(Class<Set<Track>> t) {
    super(t);
  }

  @Override
  public void serialize(
      Set<Track> tracks,
      JsonGenerator generator,
      SerializerProvider provider)
      throws IOException, JsonProcessingException {

    List<Long> ids = new ArrayList<>();
    for (Track track : tracks) {
      ids.add(track.getTrackID());
    }
    generator.writeObject(ids);
  }
}
