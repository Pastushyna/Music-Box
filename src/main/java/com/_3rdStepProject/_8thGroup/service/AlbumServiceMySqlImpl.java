package com._3rdStepProject._8thGroup.service;

import com._3rdStepProject._8thGroup.dao.AlbumsDao;
import com._3rdStepProject._8thGroup.model.Album;
import com._3rdStepProject._8thGroup.repository.AlbumsJpaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class AlbumServiceMySqlImpl implements AlbumsService {

  @Autowired
  private AlbumsDao albumsDao;

  @Autowired
  private AlbumsJpaRepository albumsJpaRepository;

  @Override
  public String testError() {
    throw new RuntimeException();
  }

  @Override
  public List<Album> getAllAlbum() {
    return albumsJpaRepository.findAll();
  }

  @Override
  public Album getAlbumById(Long albumId) {
    return albumsJpaRepository.findOne(albumId);
  }

  @Override
  public Album getAlbumByName(String albumName) {
    return albumsJpaRepository.getAlbumByAlbumName(albumName);
  }

  @Override
  @Transactional
  public void addAlbum(Album album) {
    /*albumsJpaRepository.save(album);*/
    /*return album.getAlbumId();*/
    albumsDao.addAlbum(album);
  }

  @Override
  @Transactional
  public void deleteAlbumById(Long albumId) {
    albumsJpaRepository.delete(albumId);
  }

  @Override
  @Transactional
  public void deleteCoverOfAlbum(Long albumId) {
    albumsJpaRepository.deleteCoverPathInAlbumById(albumId);
  }

  @Override
  @Transactional
  public void updateAlbumById(Long albumId, Album album) {
    Album albumTemp = albumsJpaRepository.getOne(albumId);
    albumTemp.setAlbumName(album.getAlbumName());
    albumTemp.setArtistName(album.getArtistName());
    albumTemp.setCoverPath(album.getCoverPath());
    albumTemp.setTracks(album.getTracks());
    albumsJpaRepository.saveAndFlush(albumTemp);
  }


}
