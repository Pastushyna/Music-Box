package com._3rdStepProject._8thGroup.service;

import com._3rdStepProject._8thGroup.model.User;

import java.util.List;

public interface UserService {

  List<User> getAllUsers();

  User getUserById (Long userId);

  User getUserByLogin (String userLogin);

  User getUserByEmail (String userEmail);

  Long addUser (User user);

  void deleteUserById (Long trackId);

  void updateUser(Long userId, User user);
}
