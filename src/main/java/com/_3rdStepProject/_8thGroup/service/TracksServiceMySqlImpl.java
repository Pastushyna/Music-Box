package com._3rdStepProject._8thGroup.service;

import com._3rdStepProject._8thGroup.dao.TracksDao;
import com._3rdStepProject._8thGroup.model.Track;
import com._3rdStepProject._8thGroup.repository.TracksJpaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class TracksServiceMySqlImpl implements TracksService {

  @Autowired
  private TracksDao tracksDao;

  @Autowired
  private TracksJpaRepository tracksJpaRepository;

  @Override
  public List<Track> getAllTrack() {
    return tracksJpaRepository.findAll();
  }

  @Override
  public Track getTrackById(Long trackId) {
    return tracksJpaRepository.findOne(trackId);
  }

  @Override
  public Track getTrackByName(String trackName) {
    return tracksJpaRepository.getTrackByTrackName(trackName);
  }

  @Override
  @Transactional
  public Long addTrack(Track track) {
    tracksJpaRepository.save(track);
    return track.getTrackID();
  }

  @Override
  @Transactional
  public void deleteTrackById(Long trackId) {
    tracksJpaRepository.delete(trackId);
  }

  @Override
  @Transactional
  public void updateTrack(Long trackId, Track track) {
    Track trackTemp = tracksJpaRepository.findOne(trackId);
    trackTemp.setTrackName(track.getTrackName());
    trackTemp.setTrackPath(track.getTrackPath());
    trackTemp.setAlbum(track.getAlbum());
    trackTemp.setUsers(track.getUsers());
    tracksJpaRepository.save(trackTemp);
  }
}
