package com._3rdStepProject._8thGroup.service;

import com._3rdStepProject._8thGroup.model.Track;

import java.util.List;

public interface TracksService {

  List<Track> getAllTrack();

  Track getTrackById (Long trackId);

  Track getTrackByName (String trackName);

  Long addTrack (Track track);

  void deleteTrackById (Long trackId);

  void updateTrack(Long trackId, Track track);
}
