package com._3rdStepProject._8thGroup.service;

import com._3rdStepProject._8thGroup.model.User;
import com._3rdStepProject._8thGroup.repository.UsersJpaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class UserServiceMySqlImpl implements UserService {

  @Autowired
  UsersJpaRepository usersJpaRepository;

  @Override
  public List<User> getAllUsers() {
    return usersJpaRepository.findAll();
  }

  @Override
  public User getUserById(Long userId) {
    return usersJpaRepository.findOne(userId);
  }

  @Override
  public User getUserByLogin(String userLogin) {
    return usersJpaRepository.getUserByUserLogin(userLogin);
  }

  @Override
  public User getUserByEmail(String userEmail) {
    return usersJpaRepository.getUserByUserEmail(userEmail);
  }

  @Transactional
  @Override
  public Long addUser(User user) {
    usersJpaRepository.save(user);

    return user.getUserId();
  }

  @Override
  @Transactional
  public void deleteUserById(Long trackId) {
    usersJpaRepository.delete(trackId);
  }

  @Override
  @Transactional
  public void updateUser(Long userId, User user) {
    User userTemp = usersJpaRepository.findOne(userId);
    userTemp.setUserName(user.getUserName());
    userTemp.setUserLastName(user.getUserLastName());
    userTemp.setUserEmail(user.getUserEmail());
    userTemp.setUserEnabled(user.getUserEnabled());
    userTemp.setUserPassword(user.getUserPassword());
    userTemp.setRoles(user.getRoles());
    userTemp.setTracksUser(user.getTracksUser());
    usersJpaRepository.save(userTemp);
  }
}
