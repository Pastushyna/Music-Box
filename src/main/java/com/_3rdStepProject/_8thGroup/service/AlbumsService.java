package com._3rdStepProject._8thGroup.service;

import com._3rdStepProject._8thGroup.model.Album;

import java.util.List;

public interface AlbumsService {

  String testError();

  List<Album> getAllAlbum();

  Album getAlbumById (Long albumId);

  Album getAlbumByName (String albumName);

  void addAlbum (Album album);

  void deleteAlbumById (Long albumId);

  void deleteCoverOfAlbum (Long albumId);

  void updateAlbumById(Long albumId, Album album);
}
