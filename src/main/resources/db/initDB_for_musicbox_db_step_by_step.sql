CREATE SCHEMA IF NOT EXISTS musicbox_db DEFAULT CHARACTER SET utf8;
USE `musicbox_db`;

DROP TABLE IF EXISTS musicbox_db.albums;
DROP TABLE IF EXISTS musicbox_db.users;
DROP TABLE IF EXISTS musicbox_db.tracks;
DROP TABLE IF EXISTS musicbox_db.likes;
DROP TABLE IF EXISTS musicbox_db.roles;
DROP TABLE IF EXISTS musicbox_db.privileges;

CREATE TABLE IF NOT EXISTS `musicbox_db`.`albums` (
  `album_id` BIGINT(19) NOT NULL AUTO_INCREMENT,
  `artist_name` VARCHAR(255) NULL DEFAULT 'null',
  `album_name` VARCHAR(255) NULL DEFAULT 'null',
  `album_path` VARCHAR(255) NULL DEFAULT 'null',
  PRIMARY KEY (`album_id`),
  UNIQUE INDEX `album_id_UNIQUE` (`album_id` ASC),
  UNIQUE INDEX `album_path_UNIQUE` (`album_path` ASC))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;

CREATE TABLE `musicbox_db`.`tracks` (
  `track_id` BIGINT(19) NOT NULL AUTO_INCREMENT,
  `track_name` VARCHAR(255) NULL DEFAULT 'null',
  `album_id` BIGINT(19) NOT NULL,
  PRIMARY KEY (`track_id`),
  UNIQUE INDEX `tracks_id_UNIQUE` (`track_id` ASC),
  INDEX `fk_album_id_idx` (`album_id` ASC),
  CONSTRAINT `fk_album_id`
  FOREIGN KEY (`album_id`)
  REFERENCES `musicbox_db`.`albums` (`album_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;

ALTER TABLE `musicbox_db`.`tracks`
  DROP FOREIGN KEY `fk_album_id`;
ALTER TABLE `musicbox_db`.`tracks`
  CHANGE COLUMN `album_id` `id_album` BIGINT(19) NOT NULL ,
  ADD COLUMN `track_path` VARCHAR(255) NULL AFTER `id_album`;
ALTER TABLE `musicbox_db`.`tracks`
  ADD CONSTRAINT `fk_album_id`
FOREIGN KEY (`id_album`)
REFERENCES `musicbox_db`.`albums` (`album_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `musicbox_db`.`tracks`
  DROP FOREIGN KEY `fk_album_id`;
ALTER TABLE `musicbox_db`.`tracks`
  CHANGE COLUMN `track_name` `track_name` VARCHAR(255) NULL DEFAULT NULL ,
  CHANGE COLUMN `id_album` `id_album` BIGINT(19) NULL DEFAULT NULL ;
ALTER TABLE `musicbox_db`.`tracks`
  ADD CONSTRAINT `fk_album_id`
FOREIGN KEY (`id_album`)
REFERENCES `musicbox_db`.`albums` (`album_id`)
  ON DELETE SET NULL
  ON UPDATE CASCADE;


CREATE TABLE `musicbox_db`.`users` (
  `user_id` BIGINT(19) NOT NULL AUTO_INCREMENT,
  `user_login` VARCHAR(255) NOT NULL,
  `user_email` VARCHAR(255) NOT NULL,
  `user_name` VARCHAR(255) NULL,
  `user_lastname` VARCHAR(255) NULL,
  `userPassword` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE INDEX `user_id_UNIQUE` (`user_id` ASC),
  UNIQUE INDEX `user_login_UNIQUE` (`user_login` ASC),
  UNIQUE INDEX `user_email_UNIQUE` (`user_email` ASC))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;

ALTER TABLE `musicbox_db`.`users`
  ADD COLUMN `user_enabled` TINYINT(1) NOT NULL AFTER `userPassword`;

CREATE TABLE `musicbox_db`.`tracks_favorite` (
  `user_id` BIGINT(19) NOT NULL,
  `track_id` BIGINT(19) NOT NULL,
  PRIMARY KEY (`user_id`, `track_id`),
  INDEX `fk_track_id_idx` (`track_id` ASC),
  CONSTRAINT `fk_user_id`
  FOREIGN KEY (`user_id`)
  REFERENCES `musicbox_db`.`users` (`user_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_track_id`
  FOREIGN KEY (`track_id`)
  REFERENCES `musicbox_db`.`tracks` (`track_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

CREATE TABLE `musicbox_db`.`roles` (
  `roles_id` BIGINT(19) NOT NULL AUTO_INCREMENT,
  `role_name` VARCHAR(255) NULL,
  PRIMARY KEY (`roles_id`),
  UNIQUE INDEX `roles_id_UNIQUE` (`roles_id` ASC));

CREATE TABLE `musicbox_db`.`permissions` (
  `privileges_id` BIGINT(19) NOT NULL AUTO_INCREMENT,
  `privileges_name` VARCHAR(255) NULL,
  PRIMARY KEY (`privileges_id`),
  UNIQUE INDEX `privileges_id_UNIQUE` (`privileges_id` ASC));

CREATE TABLE `musicbox_db`.`user_role` (
  `user_role_id` BIGINT(19) NOT NULL AUTO_INCREMENT,
  `id_user` BIGINT(19) NOT NULL,
  `id_role` BIGINT(19) NOT NULL,
  PRIMARY KEY (`user_role_id`),
  UNIQUE INDEX `user_role_id_UNIQUE` (`user_role_id` ASC));

CREATE TABLE `musicbox_db`.`role_permission` (
  `role_permission_id` BIGINT(19) NOT NULL AUTO_INCREMENT,
  `id_role` BIGINT(19) NOT NULL,
  `id_permission` BIGINT(19) NOT NULL,
  PRIMARY KEY (`role_permission_id`),
  UNIQUE INDEX `role_permission_id_UNIQUE` (`role_permission_id` ASC));

ALTER TABLE `musicbox_db`.`user_role`
  ADD INDEX `fk_users_id_idx` (`id_user` ASC),
  ADD INDEX `fk_roles_id_idx` (`id_role` ASC);
ALTER TABLE `musicbox_db`.`user_role`
  ADD CONSTRAINT `fk_users_id`
FOREIGN KEY (`id_user`)
REFERENCES `musicbox_db`.`users` (`user_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_roles_id`
FOREIGN KEY (`id_role`)
REFERENCES `musicbox_db`.`roles` (`roles_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `musicbox_db`.`role_permission`
  ADD INDEX `fk_roles_priv_id_idx` (`id_role` ASC),
  ADD INDEX `fk_permissions_role_id_idx` (`id_permission` ASC);
ALTER TABLE `musicbox_db`.`role_permission`
  ADD CONSTRAINT `fk_roles_priv_id`
FOREIGN KEY (`id_role`)
REFERENCES `musicbox_db`.`roles` (`roles_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_permissions_id`
FOREIGN KEY (`id_permission`)
REFERENCES `musicbox_db`.`permissions` (`privileges_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;


INSERT INTO users (user_login, user_email, user_name, user_lastname, userPassword) VALUES ('admin', 'a@a.a', 'Admin', 'Admin', 'admin'),('user', 'u@u.u', 'User', 'User', 'user');
UPDATE users SET user_enabled = 1 WHERE user_id = 1;
UPDATE users SET user_enabled = 1 WHERE user_id = 2;

INSERT INTO albums (artist_name, album_name, album_path) VALUES ('Metallica', 'Master of Puppets', 'https://iscale.iheart.com/v3/re/new_assets/5a99aa945ede6f2ef3beb98d.png');
INSERT into tracks (track_name, id_album) values ('Battery', (SELECT album_id from albums where album_name = 'Master of Puppets'));
INSERT into tracks (track_name, id_album) values ('Master of Puppets', (SELECT album_id from albums where album_name = 'Master of Puppets'));
INSERT into tracks (track_name, id_album) values ('The Thing That Should Not Be', (SELECT album_id from albums where album_name = 'Master of Puppets'));
INSERT into tracks (track_name, id_album) values ('Welcome Home (Sanitarium)', (SELECT album_id from albums where album_name = 'Master of Puppets'));
INSERT into tracks (track_name, id_album) values ('Disposable Heroes', (SELECT album_id from albums where album_name = 'Master of Puppets'));
INSERT into tracks (track_name, id_album) values ('Leper Messiah', (SELECT album_id from albums where album_name = 'Master of Puppets'));
INSERT into tracks (track_name, id_album) values ('Orion', (SELECT album_id from albums where album_name = 'Master of Puppets'));
INSERT into tracks (track_name, id_album) values ('Damage, Inc.', (SELECT album_id from albums where album_name = 'Master of Puppets'));

INSERT INTO albums (artist_name, album_name, album_path) VALUES ('NachtBlut', 'Dogma', 'https://ringmasterreviewintroduces.files.wordpress.com/2012/06/nachtblut-dogma-dark-metal-artwork.jpg');
INSERT into tracks (track_name, id_album) values ('Dogma', (SELECT album_id from albums where album_name = 'Dogma'));
INSERT into tracks (track_name, id_album) values ('Der Weg ist das Ziel', (SELECT album_id from albums where album_name = 'Dogma'));
INSERT into tracks (track_name, id_album) values ('Ich trinke Blut', (SELECT album_id from albums where album_name = 'Dogma'));
INSERT into tracks (track_name, id_album) values ('Eiskönigin', (SELECT album_id from albums where album_name = 'Dogma'));
INSERT into tracks (track_name, id_album) values ('Rache', (SELECT album_id from albums where album_name = 'Dogma'));
INSERT into tracks (track_name, id_album) values ('Mein Herz in ihren Händen', (SELECT album_id from albums where album_name = 'Dogma'));
INSERT into tracks (track_name, id_album) values ('Mordlust', (SELECT album_id from albums where album_name = 'Dogma'));
INSERT into tracks (track_name, id_album) values ('Macht', (SELECT album_id from albums where album_name = 'Dogma'));
INSERT into tracks (track_name, id_album) values ('Bußsakrament', (SELECT album_id from albums where album_name = 'Dogma'));
INSERT into tracks (track_name, id_album) values ('Vulva', (SELECT album_id from albums where album_name = 'Dogma'));
INSERT into tracks (track_name, id_album) values ('Die Schritte', (SELECT album_id from albums where album_name = 'Dogma'));

INSERT INTO albums (artist_name, album_name, album_path) VALUES ('Joe Satriani', 'What Happens Next', 'https://static-eu.insales.ru/images/products/1/337/143417681/joe-satriani-what-happens-next.jpg');
INSERT into tracks (track_name, id_album) values ('Energy', (SELECT album_id from albums where album_name = 'What Happens Next'));
INSERT into tracks (track_name, id_album) values ('Catbot', (SELECT album_id from albums where album_name = 'What Happens Next'));
INSERT into tracks (track_name, id_album) values ('Thunder High on the Mountain', (SELECT album_id from albums where album_name = 'What Happens Next'));
INSERT into tracks (track_name, id_album) values ('Cherry Blossoms', (SELECT album_id from albums where album_name = 'What Happens Next'));
INSERT into tracks (track_name, id_album) values ('Righteous', (SELECT album_id from albums where album_name = 'What Happens Next'));
INSERT into tracks (track_name, id_album) values ('Smooth Soul', (SELECT album_id from albums where album_name = 'What Happens Next'));
INSERT into tracks (track_name, id_album) values ('Headrush', (SELECT album_id from albums where album_name = 'What Happens Next'));
INSERT into tracks (track_name, id_album) values ('Looper', (SELECT album_id from albums where album_name = 'What Happens Next'));
INSERT into tracks (track_name, id_album) values ('What Happens Next', (SELECT album_id from albums where album_name = 'What Happens Next'));
INSERT into tracks (track_name, id_album) values ('Super Funky Badass', (SELECT album_id from albums where album_name = 'What Happens Next'));
INSERT into tracks (track_name, id_album) values ('Invisible', (SELECT album_id from albums where album_name = 'What Happens Next'));
INSERT into tracks (track_name, id_album) values ('Forever and Ever', (SELECT album_id from albums where album_name = 'What Happens Next'));

INSERT INTO albums (artist_name, album_name, album_path) VALUES ('Five Finger Death Punch', 'American Capitalist', 'https://upload.wikimedia.org/wikipedia/ru/6/65/American_Capitalist.jpg');
INSERT into tracks (track_name, id_album) values ('American Capitalist', (SELECT album_id from albums where album_name = 'American Capitalist'));
INSERT into tracks (track_name, id_album) values ('Under and Over It', (SELECT album_id from albums where album_name = 'American Capitalist'));
INSERT into tracks (track_name, id_album) values ('The Pride', (SELECT album_id from albums where album_name = 'American Capitalist'));
INSERT into tracks (track_name, id_album) values ('Coming Down', (SELECT album_id from albums where album_name = 'American Capitalist'));
INSERT into tracks (track_name, id_album) values ('Menace', (SELECT album_id from albums where album_name = 'American Capitalist'));
INSERT into tracks (track_name, id_album) values ('Generation Dead', (SELECT album_id from albums where album_name = 'American Capitalist'));
INSERT into tracks (track_name, id_album) values ('Back for More', (SELECT album_id from albums where album_name = 'American Capitalist'));
INSERT into tracks (track_name, id_album) values ('Remember Everything', (SELECT album_id from albums where album_name = 'American Capitalist'));
INSERT into tracks (track_name, id_album) values ('Wicked Ways', (SELECT album_id from albums where album_name = 'American Capitalist'));
INSERT into tracks (track_name, id_album) values ('If I Fall', (SELECT album_id from albums where album_name = 'American Capitalist'));
INSERT into tracks (track_name, id_album) values ('100 Ways to Hate', (SELECT album_id from albums where album_name = 'American Capitalist'));


