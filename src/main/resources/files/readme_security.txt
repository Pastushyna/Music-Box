.antMatchers().permitAll()
                .antMatchers(HttpMethod.GET,"/api/albums/","/api/albums/**","/api/tracks"
            ,"/api/tracks/**", "/api/users/me", "/api/tracks?liked=true").hasAnyRole()
                .antMatchers(HttpMethod.POST,"/api/tracks/**/like").hasAnyRole()
                .antMatchers(HttpMethod.PUT, "/api/albums/**", "/api/tracks/**"
            ,"/api/tracks/**/cover").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST, "/api/tracks", "/api/albums").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/api/tracks/**", "/api/albums/**").hasRole("ADMIN")
                .antMatchers("/api/auth/**").permitAll()
                .antMatchers("/api/users/checkUsernameAvailability"
                                     ,"/api/users/checkEmailAvailability").permitAll()