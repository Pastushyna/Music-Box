import React from 'react';
import {connect} from 'react-redux';

const Audio = (props) => {

    return props.player.status === "START" ?
        <audio src={props.player.trackPath} autoPlay controls /> : null

}

const mapStateToProps = (state) => ({
    player: state.player
})



export default connect(mapStateToProps)(Audio);