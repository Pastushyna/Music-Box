import { combineReducers } from 'redux';
import albumReducer from './albumsReducer';
import playerReducer from './playerReducer'

export default combineReducers({
    albums : albumReducer,
    player : playerReducer
});
