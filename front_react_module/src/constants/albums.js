export const ALBUMS_LOADED = 'ALBUMS_LOADED';
export const ALBUM_ADD = 'ALBUM_ADD';
export const ALBUM_DELETE = 'ALBUM_DELETE';