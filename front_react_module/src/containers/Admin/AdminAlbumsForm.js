import React,{Component} from 'react';
import {connect} from 'react-redux';
import {loadAlbums} from '../../actions/albumsActions';


class AdminAlbumsForm extends Component {

    render() {
        return (
            <div>
                <form  onSubmit={this.handleSubmit.bind(this)} >
                    Title:
                    <input type="text" name='title' id='title'  onChange={this.handlerTitle.bind(this)}  placeholder='Album title'  />
                    <button>Submit</button>
                </form>
            </div>
        )
    }

}

const  mapDispatchToProps = dispatch=> {
    return{
        loadData: () => dispatch(loadAlbums())
    }
}
const mapStateToProps = state =>{
    return {
        albums: state.albums
    }
}


export default  connect(mapStateToProps, mapDispatchToProps)(AdminAlbumsForm);