import React, {Component} from 'react';
import {connect} from 'react-redux'
import {ALBUM_ADD, ALBUM_DELETE} from "../../constants/albums";
import {deleteAlbum, addAlbum} from "../../actions/albumsActions";



class AdminAlbums extends Component {

    handleClick = () => {

        const artist = document.getElementById('artist_name').value;
        const title = document.getElementById('album_title').value;
        const url = document.getElementById('cover_url').value;

        fetch('http://localhost:8080/api/albums/secret/map', {

            headers: {
                'Accept': 'application/json',
                'content-type': 'application/json'
            },
            method: 'POST',
            body: JSON.stringify({ artistName:artist, albumName:title, coverPath:url})

        })
    }

    render() {
        return (
            <div>

                <h2>Admin Albums</h2>
                <input type="text" placeholder="Artist name" id="artist_name"/>
                <input type="text" placeholder="Album title" id="album_title"/>
                <input type="text" placeholder="Album cover url" id="cover_url"/>


                <button onClick={this.handleClick}>add Album</button>

                <ul>
                    {this.props.albums.map((album, index) =>
                        <li key={index}>{album.albumName}
                            <button onClick={() => this.props.onDeleteAlbum(album.albumId)}>x</button>
                        </li>)}
                </ul>
            </div>
        );
    }
}


export default connect(
    state => ({
        albums: state.albums
    }),
    dispatch => ({
        addAlbum: (album_name) => {
            dispatch({type: ALBUM_ADD, payload: album_name});
        },
        onDeleteAlbum: (album_id) => dispatch(deleteAlbum(album_id))


    }))(AdminAlbums);
