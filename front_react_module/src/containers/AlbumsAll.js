import React, {Component} from 'react';
import { Link, Route, Switch } from 'react-router-dom';
import {connect} from 'react-redux';
import AlbumSingle from "./AlbumSingle";

const AlbumsAll = props => {
console.log(props);
  return (
    <div>
      {props.albums.map(album =>
        (<div className = "album" key={album.albumName}>
          <header>{album.albumName}</header>
          <Link to = {`/albums/${album.albumId}`}><img className="album__cover" src={album.coverPath}/></Link>
        </div>)
      )}
    </div>

  )
  /*onClick={() => this.redirectToTarget(album.albumId)}*/
        /*const {albumId, albumName, coverPath} = props.albums;

        return (
            <div className="album">
                <header>{albumName}</header>
                <img onClick={() => this.redirectToTarget(albumId)} className="album__cover"
                src={coverPath}/>
            </div>
        );*/

}


const redirectToTarget = id => {
    window.location.href = "/albums/" + id;
}

const mapStateToProps = state => ({
   albums: state.albums
});

export default connect(mapStateToProps)(AlbumsAll);