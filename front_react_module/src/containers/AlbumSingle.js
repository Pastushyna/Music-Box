import React from 'react';
import { Link, Route, Switch } from 'react-router-dom';
import {connect} from 'react-redux';
import TrackSingle from './TrackSingle'
import PropTypes from 'prop-types';


const AlbumSingle = (props) => {

    console.log(props.album, 'album');


    return props.album ? (
    <div className="album" >
        <h2>{props.album.albumName}</h2>
        <img className="album__cover" src={props.album.coverPath}/>
        <ul>{/*v TrackSingle mi pereedaem props, name luboy*/}
            {props.album.tracks.map(track => <TrackSingle track={track} key={track.trackName}/>)}
        </ul>
    </div>

    ) : null
};

/*AlbumSingle.propTypes = {
    album: PropTypes.shape({
        albumId: PropTypes.number.isRequired,
        alb
    }).isRequired
}*/

const mapStateToProps = (state, ownProps) => ({
    album: state.albums.find(album => +ownProps.match.params.albumId === album.albumId)
});

export default connect(mapStateToProps)(AlbumSingle);