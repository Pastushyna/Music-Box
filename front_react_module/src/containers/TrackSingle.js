import React from 'react';
import {connect} from 'react-redux';
import {playTrack, stopTrack} from "../actions/playerActions";

const TrackSingle = (props) => (


    <div className = "track" key={props.track.trackName}>
            <button className="button" onClick={() => props.playTrack(props.tracks.trackPath)}>
                play
            </button>
        <header>{props.track.trackName}</header>

    </div>



);

//get data from store
const mapToStateProp = () => {
    return null;
}

// post actions
const mapDispatchToProps = dispatch => ({
    playTrack: (trackPath) => dispatch(playTrack(trackPath)),
    stopTrack: () => dispatch(stopTrack())
});

export default connect(null, mapDispatchToProps)(TrackSingle);