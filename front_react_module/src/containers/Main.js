import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from './Home'
import Albums from './Albums';
import Tracks from './Tracks';
import Login from './Login';
import User from './User';

const Main = () => (
    <main>
        <Switch>
            <Route exact path = '/' component = {Home}/>
            <Route path = '/api/albums/' component = {Albums} />
            <Route path = '/api/tracks/' component = {Tracks} />
            <Route path = '/login' component = {Login} />
            <Route path = '/api/users/me' component = {User} />
        </Switch>
    </main>
);

export default Main;