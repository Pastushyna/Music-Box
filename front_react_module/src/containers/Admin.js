import React from 'react';
import {Switch, Route, Link} from 'react-router-dom'
import AdminAlbums from './Admin/AdminAlbums'
import AdminAlbumsForm from "./Admin/AdminAlbumsForm";
import AdminTracks from "./Admin/AdminTracks";
import AdminTracksForm from "./Admin/AdminTracksForm";


class Admin extends React.Component {
    render() {
        const path = this.props.match.path

        return (<div>
                <h2>Admin menu</h2>
                <nav>
                    <Link to='/admin/albums/'>Albums</Link>
                    <br/>
                    <Link to='/admin/tracks/'>Tracks</Link>
                </nav>
                <Switch>
                    <Route exact path='/admin/' component={AdminAlbums}/>
                    <Route exact path='/admin/albums/' component={AdminAlbums}/>
                    <Route path='/admin/albums/create' component={AdminAlbumsForm}/>
                    <Route path='/admin/albums/:id' component={AdminAlbumsForm}/>
                    <Route exact path='/admin/tracks/' component={AdminTracks}/>
                    <Route path='/admin/tracks/create' component={AdminTracksForm}/>
                    <Route path='/admin/tracks/:id' component={AdminTracksForm}/>
                </Switch>
            </div>
        );
    }
}
export default Admin
