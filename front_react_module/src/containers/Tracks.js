import React from 'react';
import { Switch, Route } from 'react-router-dom';
import TracksAll from './TracksAll';
import TrackSingle from './TrackSingle';

const Tracks = () => (
    <Switch>
        <Route exact path = '/tracks/' component = {TracksAll}/>
        <Route path = '/tracks/id' component = {TrackSingle} />
    </Switch>
);

export default Tracks;