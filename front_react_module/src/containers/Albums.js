import React from 'react';
import { Switch, Route } from 'react-router-dom';
import AlbumsAll from './AlbumsAll';
import AlbumSingle from './AlbumSingle';

const Albums = () => {
    return (
        <Switch>
                <Route exact path = '/api/albums/' component = {AlbumsAll}/>
            <Route path = '/albums/id' component = {AlbumSingle} />
        </Switch>
    );
}

export default Albums;