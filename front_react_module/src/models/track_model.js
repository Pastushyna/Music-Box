import PropTypes from 'prop-types';

export default {
    trackId : PropTypes.number,
    trackName : PropTypes.string,
    trackPath : PropTypes.string,
}