import {PLAYER_START, PLAYER_STOP} from "../constants/playerConstants";

export const playTrack = trackPath => ({type: PLAYER_START, payload: trackPath})

export const stopTrack = () => ({type: PLAYER_STOP})