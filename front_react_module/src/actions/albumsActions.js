import {ALBUMS_LOADED} from "../constants/albums";
import {ALBUM_DELETE} from "../constants/albums";
import {ALBUM_ADD} from "../constants/albums";
import axios from 'axios';



export const loadAlbums = () => dispatch => {

    fetch('http://localhost:8080/api/albums/')
        .then(res => res.json())
        .then(data => dispatch({type: ALBUMS_LOADED, payload: data}))
}


export function addAlbum(album_name) {
    return {
        type: ALBUM_ADD,
        album_name

    };
}

export const deleteAlbum = (id) => dispatch => {

    axios.delete(`http://localhost:8080/api/albums/${id}`)
        .then(data => dispatch({type: ALBUM_DELETE, payload: id}))
        .catch(data => dispatch({type: ALBUM_DELETE, payload: id}))

}
