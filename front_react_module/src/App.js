import React from "react";
import { Route, Link, Switch } from 'react-router-dom';
import {connect} from 'react-redux';
import {loadAlbums} from "./actions/albumsActions";
import AlbumsAll from "./containers/AlbumsAll";
import AlbumSingle from "./containers/AlbumSingle";
import Login from "./containers/Login";
import Audio from "./components/Audio"
import Admin from "./containers/Admin";


class App extends React.Component {
    /* -- like a Kostya M -- componentDidMount() {
        if (this.props.albums.length === 0) {
            fetch('http://localhost:8080/api/albums/')
                .then(res => res.json())
                .then(albums => this.props.loadData(albums))
        }
    }*/
    componentDidMount() {
        console.log('app')
        if (this.props.albums.length === 0) {
            this.props.loadData();
        }
    }


    render () {
        return (
            <div>
                <header>
                    <Link to="/albums">Albums</Link>
                    <Link to="/favorites">Favorites</Link>
                    <Link to="/admin">Admin</Link>
                    <Link to="/login">Login</Link>
                </header>
                <Audio />
                <Switch>
                    <Route exact path="/" component={AlbumsAll}/>
                    <Route exact path="/albums" component={AlbumsAll}/>
                    <Route exact path="/albums/:albumId" component={AlbumSingle}/>
                    <Route exact path="/admin" component={Admin}/>
                    <Route exact path="/login" component={Login}/>
                </Switch>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    albums: state.albums
});

const mapDispatchToProps = dispatch => ({
    loadData: () => dispatch(loadAlbums())
});

export default connect(mapStateToProps, mapDispatchToProps)(App)

