
CREATE TABLE `albums` (
  `album_id` bigint(19) NOT NULL AUTO_INCREMENT,
  `artist_name` varchar(255) DEFAULT 'null',
  `album_name` varchar(255) DEFAULT 'null',
  `album_path` varchar(255) DEFAULT 'null',
  PRIMARY KEY (`album_id`),
  UNIQUE KEY `album_id_UNIQUE` (`album_id`),
  UNIQUE KEY `album_path_UNIQUE` (`album_path`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8

CREATE TABLE `tracks` (
  `track_id` bigint(19) NOT NULL AUTO_INCREMENT,
  `track_name` varchar(255) DEFAULT 'null',
  `id_album` bigint(19) NOT NULL,
  `track_path` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`track_id`),
  UNIQUE KEY `tracks_id_UNIQUE` (`track_id`),
  KEY `fk_album_id_idx` (`id_album`),
  CONSTRAINT `fk_album_id` FOREIGN KEY (`id_album`) REFERENCES `albums` (`album_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8

CREATE TABLE `tracks_favorite` (
  `user_id` bigint(19) NOT NULL,
  `track_id` bigint(19) NOT NULL,
  PRIMARY KEY (`user_id`,`track_id`),
  KEY `fk_track_id_idx` (`track_id`),
  CONSTRAINT `fk_track_id` FOREIGN KEY (`track_id`) REFERENCES `tracks` (`track_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `users` (
  `user_id` bigint(19) NOT NULL AUTO_INCREMENT,
  `user_login` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `user_lastname` varchar(255) DEFAULT NULL,
  `userPassword` varchar(255) NOT NULL,
  `user_enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`),
  UNIQUE KEY `user_login_UNIQUE` (`user_login`),
  UNIQUE KEY `user_email_UNIQUE` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8